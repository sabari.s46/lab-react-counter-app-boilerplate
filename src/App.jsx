import { Component } from "react";
import "./App.css";

export default class AppClass extends Component {
  // declaring state
  state = {
    count: 0,
  };
  render() {
    // accessing the state value
    const count = this.state.count;
    return (
      <div className="App">
        <h1 className="Heading">Counter App</h1>
        <h1 className="count">{count} </h1>

        <div>
          <button
            onClick={() => this.setState({ count: this.state.count + 1 })}
          >
            +
          </button>{" "}
          <button
            onClick={() => this.setState({ count: this.state.count - 1 })}
          >
            -
          </button>{" "}
          <button onClick={() => this.setState({ count: 0 })}>Reset</button>
        </div>
      </div>
    );
  }
}
